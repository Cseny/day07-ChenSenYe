# O

- Integration Test on API. 
- Service layer with TDD


# R

This is a fruitful day.

# I

- Not very familiar with Service layer with TDD yet.

# D

- Learned the Integration test on API, which can make API testing faster and safer.
- Service layer with TDD has lower testing costs and is the foundation for ensuring project code quality.