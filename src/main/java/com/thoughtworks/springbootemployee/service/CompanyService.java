package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompaniesMemoryRepository;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompaniesMemoryRepository companiesMemoryRepository;

    @Autowired
    private EmployeesMemoryRepository employeesMemoryRepository;

    public Company createCompanies(Company company) {
        return companiesMemoryRepository.createCompanies(company);
    }

    public List<Company> findAll() {
        return companiesMemoryRepository.findAll();
    }


    public Company findSpecificCompany(Long id) {
        return companiesMemoryRepository.findSpecificCompany(id);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeesMemoryRepository.findEmployeesByCompanyId(id);
    }

    public List<Company> queryCompany(Long page, Long size) {
        return companiesMemoryRepository.queryCompany(page,size);
    }

    public Company updateAnCompany(Long id, Company latestCompany) {
        Company originCompany = companiesMemoryRepository.findSpecificCompany(id);
        if (originCompany != null) {
            originCompany.setName(latestCompany.getName());
            return companiesMemoryRepository.updateAnCompany(id, originCompany);
        } else {
            throw new NotFoundException();
        }
    }

    public void deleteCompany(Long id) {
        Company toRemovedCompany = companiesMemoryRepository.findSpecificCompany(id);
        if (toRemovedCompany != null) {
            companiesMemoryRepository.deleteCompany(toRemovedCompany);
        } else {
            throw new NotFoundException();
        }
    }
}
