package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeesService {
    private final EmployeesMemoryRepository employeesMemoryRepository;

    public EmployeesService(EmployeesMemoryRepository employeesMemoryRepository) {
        this.employeesMemoryRepository = employeesMemoryRepository;
    }

    public Employee createEmployee(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new AgeIsInvalidException();
        }
        return employeesMemoryRepository.createEmployee(employee);
    }

    public List<Employee> findAll() {
        return employeesMemoryRepository.findAll();
    }

    public Employee findSpecificEmployee(Long id) {
        Employee specificEmployee = employeesMemoryRepository.findSpecificEmployee(id);
        if (specificEmployee == null) {
            throw new NotFoundException();
        }
        return specificEmployee;
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeesMemoryRepository.getEmployeeByGender(gender);
    }


    public Employee updateAnEmployee(Long id, Employee latestEmployee) {
        Employee originEmployee = employeesMemoryRepository.findSpecificEmployee(id);
        if (originEmployee != null && originEmployee.getActive()) {
            originEmployee.setSalary(latestEmployee.getSalary());
            originEmployee.setAge(latestEmployee.getAge());
            return employeesMemoryRepository.updateAnEmployee(id, originEmployee);
        } else {
            throw new NotFoundException();
        }
    }

    public void deleteEmployee(Long id) {
        Employee toRemovedEmployee = employeesMemoryRepository.findSpecificEmployee(id);
        if (toRemovedEmployee != null) {
            employeesMemoryRepository.deleteEmployee(toRemovedEmployee);
        } else {
            throw new NotFoundException();
        }
    }

    public List<Employee> queryEmployee(Long page, Long size) {
        return employeesMemoryRepository.queryEmployee(page, size);
    }
}
