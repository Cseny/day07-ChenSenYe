package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import com.thoughtworks.springbootemployee.service.EmployeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeesController {

    @Autowired
    EmployeesService employeesService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee) {
        return employeesService.createEmployee(employee);
    }

    @GetMapping
    public List<Employee> findAll() {
        return employeesService.findAll();
    }

    @GetMapping("/{id}")
    public Employee findSpecificEmployee(@PathVariable Long id) {
        return employeesService.findSpecificEmployee(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeeByGender(@RequestParam("gender") String gender) {
        return employeesService.getEmployeeByGender(gender);
    }

    @PutMapping("/{id}")
    public Employee updateAnEmployee(@PathVariable("id") Long id, @RequestBody Employee latestEmployee) {
        return employeesService.updateAnEmployee(id,latestEmployee);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeesService.deleteEmployee(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> queryEmployee(@RequestParam("page") Long page, @RequestParam("size") Long size) {
        return employeesService.queryEmployee(page,size);
    }
}
