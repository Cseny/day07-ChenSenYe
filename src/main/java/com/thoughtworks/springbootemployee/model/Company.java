package com.thoughtworks.springbootemployee.model;

import lombok.Data;

import java.util.List;

@Data
public class Company {

    private Long id;
    private String name;

    public Company(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
