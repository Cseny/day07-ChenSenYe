package com.thoughtworks.springbootemployee.model;


import lombok.Data;

@Data
public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Double salary;
    private Long companyId;
    private Boolean active;

    public Employee(Long id, String name, Integer age, String gender, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }
}
