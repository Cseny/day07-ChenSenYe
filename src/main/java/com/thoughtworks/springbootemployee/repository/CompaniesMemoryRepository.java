package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompaniesMemoryRepository {
    private final List<Company> companies = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public Company createCompanies(Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findSpecificCompany(Long id) {
        return companies.stream()
                .filter(company -> id.equals(company.getId()))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Company> queryCompany(Long page, Long size) {
        return companies.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company updateAnCompany(Long id, Company originCompany) {
        companies.removeIf(employee -> employee.getId().equals(id));
        companies.add(originCompany);
        return originCompany;
    }

    public void deleteCompany(Company company) {
        companies.remove(company);
    }

    public void clearAll() {
        companies.clear();
    }
}
