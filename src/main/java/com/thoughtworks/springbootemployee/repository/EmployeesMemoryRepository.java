package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeesMemoryRepository {
    private final List<Employee> employees = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public Employee createEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employee.setActive(true);
        employees.add(employee);
        return employee;
    }

    
    public List<Employee> findAll() {
        return employees;
    }

    public Employee findSpecificEmployee(Long id) {
        return employees.stream()
                .filter(employee -> id.equals(employee.getId()))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> gender.equals(employee.getGender()))
                .collect(Collectors.toList());
    }

    public Employee updateAnEmployee(Long id, Employee originEmployee) {
        employees.removeIf(employee -> employee.getId().equals(id));
        employees.add(originEmployee);
        return originEmployee;
    }

    public void deleteEmployee(Employee employee) {
        employees.remove(employee);
        employee.setActive(false);
    }

    public List<Employee> queryEmployee(Long page, Long size) {
        return employees.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> findEmployeesByCompanyId(Long companyId) {
        return employees.stream().filter(employee -> companyId.equals(employee.getCompanyId())).collect(Collectors.toList());
    }

    public void clearAll() {
        employees.clear();
    }
}
