package com.thoughtworks.springbootemployee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ErrorControllerAdvice {
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Exception> exceptionHandler(HttpServletRequest request, Exception exception) {
        if (exception instanceof NotFoundException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception);
    }
}
