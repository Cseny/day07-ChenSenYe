package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeContollerTest {
    @Autowired
    private EmployeesMemoryRepository employeeRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void clearEmployeeData() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
        Employee john = new Employee(null, "John Swith", 32, "Male", 5000.0);
        employeeRepository.createEmployee(john);

        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_employees_filter_by_gender_when_perform_findByGender_given_two_employee_with_different_gender() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        Employee emily = new Employee(null, "Emily Smith", 32, "Female", 5000.0);
        employeeRepository.createEmployee(john);
        employeeRepository.createEmployee(emily);

        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Male"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        Employee john = new Employee(null, "John", 22, "Male", 5000.0);
        String johnJson = new ObjectMapper().writeValueAsString(john);
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("John"))
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(5000.0));
    }

    @Test
    void should_return_specific_employee_when_get_a_specific_employee_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John Swith", 32, "Male", 5000.0);
        employeeRepository.createEmployee(john);

        client.perform(MockMvcRequestBuilders.get("/employees/{id}", john.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(john.getId()))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value(john.getGender()))
                .andExpect(jsonPath("$.salary").value(john.getSalary()));
    }

    @Test
    void should_update_specific_employee_when_put_a_specific_employee_given_employee() throws Exception {
        Employee john = new Employee(null, "John Swith", 32, "Male", 5000.0);
        Employee newJohn = new Employee(null, "John Swith", 33, "Male", 10000.0);
        employeeRepository.createEmployee(john);

        String johnJson = new ObjectMapper().writeValueAsString(newJohn);

        client.perform(MockMvcRequestBuilders.put("/employees/{id}", john.getId()).contentType(MediaType.APPLICATION_JSON).content(johnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(john.getId()))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(newJohn.getAge()))
                .andExpect(jsonPath("$.gender").value(john.getGender()))
                .andExpect(jsonPath("$.salary").value(newJohn.getSalary()))
                .andExpect(result -> assertEquals(employeeRepository.findSpecificEmployee(john.getId()).getSalary(), newJohn.getSalary()))
                .andExpect(result -> assertEquals(employeeRepository.findSpecificEmployee(john.getId()).getAge(), newJohn.getAge()));
    }

    @Test
    void should_delete_specific_employee_when_delete_a_specific_employee_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John Swith", 32, "Male", 5000.0);
        employeeRepository.createEmployee(john);

        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", john.getId()))
                .andExpect(status().isNoContent())
                .andExpect(result -> assertThrows(NotFoundException.class, () -> employeeRepository.findSpecificEmployee(john.getId())));
    }

    @Test
    void should_return_specific_employees_when_query_with_specific_page_and_size_given_page_and_size() throws Exception {
        for (int i = 0; i < 10; i++) {
            employeeRepository.createEmployee(new Employee(null, "John Swith" + i, 32, "Male", 5000.0));
        }
        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(5)))
                .andExpect(jsonPath("$[0].name").value("John Swith0"))
                .andExpect(jsonPath("$[1].name").value("John Swith1"))
                .andExpect(jsonPath("$[2].name").value("John Swith2"))
                .andExpect(jsonPath("$[3].name").value("John Swith3"))
                .andExpect(jsonPath("$[4].name").value("John Swith4"));
    }
}
