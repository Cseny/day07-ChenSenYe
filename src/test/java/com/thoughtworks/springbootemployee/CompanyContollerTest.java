package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompaniesMemoryRepository;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyContollerTest {
    @Autowired
    private CompaniesMemoryRepository companiesMemoryRepository;

    @Autowired
    private EmployeesMemoryRepository employeesMemoryRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void clearCompanyAndEmployeeData() {
        companiesMemoryRepository.clearAll();
        employeesMemoryRepository.clearAll();
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        Company company = new Company(null, "OOCL");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("OOCL"));
    }

    @Test
    void should_return_companies_when_getAllCompanies_given_companies() throws Exception {
        Company company = new Company(null, "OOCL");
        companiesMemoryRepository.createCompanies(company);

        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(company.getId()))
                .andExpect(jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_return_specific_company_when_get_a_specific_company_given_company_id() throws Exception {
        Company company = new Company(null, "OOCL");
        companiesMemoryRepository.createCompanies(company);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(company.getId()))
                .andExpect(jsonPath("$.name").value(company.getName()));
    }

    @Test
    void should_return_specific_employees_when_find_employees_by_company_id_given_company_id() throws Exception {
        Company company = new Company(null, "OOCL");
        companiesMemoryRepository.createCompanies(company);
        Employee john = new Employee(null, "John Swith", 32, "Male", 5000.0);
        Employee mike = new Employee(null, "Mike Swith", 32, "Male", 5000.0);
        Employee tom = new Employee(null, "Tom Swith", 32, "Male", 5000.0);
        john.setCompanyId(1L);
        mike.setCompanyId(1L);
        tom.setCompanyId(1L);
        employeesMemoryRepository.createEmployee(john);
        employeesMemoryRepository.createEmployee(mike);
        employeesMemoryRepository.createEmployee(tom);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", company.getId()))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_specific_companies_when_query_with_specific_page_and_size_given_page_and_size() throws Exception {
        for (int i = 0; i < 10; i++) {
            companiesMemoryRepository.createCompanies(new Company(null, "OOCL" + i));
        }
        client.perform(MockMvcRequestBuilders.get("/companies").param("page", "1").param("size", "3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name").value("OOCL0"))
                .andExpect(jsonPath("$[1].name").value("OOCL1"))
                .andExpect(jsonPath("$[2].name").value("OOCL2"));
    }

    @Test
    void should_update_specific_company_when_put_a_specific_company_given_company() throws Exception {
        Company company = new Company(null, "OOCL");
        Company newCompany = new Company(null, "CargoSmart");
        companiesMemoryRepository.createCompanies(company);

        String companyJson = new ObjectMapper().writeValueAsString(newCompany);

        client.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId()).contentType(MediaType.APPLICATION_JSON).content(companyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(company.getId()))
                .andExpect(jsonPath("$.name").value(company.getName()))
                .andExpect(result -> assertEquals(companiesMemoryRepository.findSpecificCompany(company.getId()).getName(), newCompany.getName()));
    }

    @Test
    void should_delete_specific_company_when_delete_a_specific_company_given_company_id() throws Exception {
        Company company = new Company(null, "OOCL");
        companiesMemoryRepository.createCompanies(company);

        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                .andExpect(status().isNoContent())
                .andExpect(result -> assertThrows(NotFoundException.class, () -> companiesMemoryRepository.findSpecificCompany(company.getId())));
    }
}
