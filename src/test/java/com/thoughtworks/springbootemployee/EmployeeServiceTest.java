package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeesMemoryRepository;
import com.thoughtworks.springbootemployee.service.EmployeesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    EmployeesMemoryRepository mockEmployeeRepository = mock(EmployeesMemoryRepository.class);

    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeesService employeeService = new EmployeesService(mockEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 15, "Female", 6000d);
        Assertions.assertThrows(AgeIsInvalidException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_create_successfully_when_create_employee_given_age_is_valid() {
        EmployeesService employeeService = new EmployeesService(mockEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 20, "Female", 6000d);
        given(mockEmployeeRepository.createEmployee(any())).willReturn(employee);

        employeeService.createEmployee(employee);

        verify(mockEmployeeRepository).createEmployee(argThat(myEmployee -> {
            assertThat(myEmployee.getName()).isEqualTo(employee.getName());
            assertThat(myEmployee.getAge()).isEqualTo(employee.getAge());
            return true;
        }));
    }

    @Test
    void should_set_in_active_when_create_employee_given_an_employee() {
        EmployeesService employeeService = new EmployeesService(mockEmployeeRepository);
        Employee employee = new Employee(null, "sj", 20, "Female", 6000d);

        employeeService.createEmployee(employee);

        verify(mockEmployeeRepository, times(1)).createEmployee(employee);
    }

    @Test
    void should_set_in_active_when_delete_employee_given_an_active_employee() {
        EmployeesService employeeService = new EmployeesService(mockEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 20, "Female", 6000d);
        given(mockEmployeeRepository.findSpecificEmployee(any())).willReturn(employee);

        employeeService.deleteEmployee(1L);

        verify(mockEmployeeRepository, times(1)).deleteEmployee(employee);
    }

    @Test
    void should_verify_employee_active_when_update_employee_given_an_employee_no_active() {
        EmployeesService employeeService = new EmployeesService(mockEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 20, "Female", 6000d);
        employee.setActive(false);
        given(mockEmployeeRepository.findSpecificEmployee(any())).willReturn(employee);

        Assertions.assertThrows(NotFoundException.class, () -> employeeService.updateAnEmployee(employee.getId(), employee));
    }
}
